let http = require('http');
let port = 4000;

http.createServer((req, res) => {

    if(req.url == "/" && req.method == "GET"){
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.end('Welcome to booking system!');
	}

    if(req.url == "/profile" && req.method == "GET"){
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.end('Welcome to to your profile!');
	}

    if(req.url == "/courses" && req.method == "GET"){
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.end('Here are our courses!');
	}

    if(req.url == "/addCourse" && req.method == "POST"){
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.end('Add course to our resources.');
	}

    if(req.url == "/updateCourse" && req.method == "PUT"){
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.end('Update a course to our resources.');
	}

    if(req.url == "/archiveCourse" && req.method == "DELETE"){
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.end('Archive a course to our resources.');
	}
}).listen(port);

console.log(`Server is running at port ${port}...`);